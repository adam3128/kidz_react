import React from 'react';
import {
    Link
} from 'react-router-dom'


class Button extends React.Component {

    render() {

        if (!this.props.button) {
            return null;
        }

        return (

            <div className="buttons">
                <div className={`button-item ${this.props.button.colour}`}>
                    <h3>{this.props.button.introduction}</h3>
                    <Link to={this.props.button.link}>{this.props.button.title}</Link>
                </div>
            </div>

        )
    }

}


export default Button;