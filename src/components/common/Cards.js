import React from 'react';


class Cards extends React.Component {

    render() {

        if (!this.props.cards) {
            return null;
        }

        return (
            <div className="cards">

                {
                    this.props.cards.map(function (card, i) {
                        return <div className="card-item" key={i}>
                            <div className="card-title">
                                {card.title}
                            </div>

                            <div className="card-image">
                                <img src={card.imageUrl} alt=""/>
                            </div>

                            <div className="card-content">
                                {card.content}
                            </div>
                        </div>

                    }, this)
                }

            </div>

        )
    }

}


export default Cards;