import React from 'react';
import Button from 'components/common/Button';


class NewsTestimonials extends React.Component {

    render() {

        return (
            <div className="news-testimonials">
                <div className="container">
                    <div className="row">

                        <div className="post-items">

                            <h2>News & Updates</h2>

                            {
                                this.props.newsData.NewsItems.map(function (newsItem, i) {
                                    return <div className="post-item" key={i}>
                                        <div className="row">
                                            <div className="post-item-image">
                                                <img src={newsItem.imageUrl} alt={newsItem.imageAlt}/>
                                            </div>
                                            <div className="post-item-content">
                                                <h3>{newsItem.title}</h3>
                                                <p>{newsItem.content}</p>
                                                <p>{newsItem.strapline}</p>
                                            </div>
                                        </div>
                                    </div>
                                }, this)
                            }

                            <div className="post-items-read-more">
                                <Button button={this.props.newsData.NewsButton}/>
                            </div>

                        </div>

                        <div className="post-items">

                            <h2>Testimonials</h2>

                            {
                                this.props.newsData.TestimonialItems.map(function (testimonialItem, i) {
                                    return <div className="post-item" key={i}>
                                        <div className="row">
                                            <div className="post-item-image">
                                                <img src={testimonialItem.imageUrl} alt={testimonialItem.imageAlt}/>
                                            </div>
                                            <div className="post-item-content">
                                                <h3>{testimonialItem.title}</h3>
                                                <p>{testimonialItem.content}</p>
                                                <p>{testimonialItem.strapline}</p>
                                            </div>
                                        </div>
                                    </div>
                                }, this)
                            }

                            <div className="post-items-read-more">
                                <Button button={this.props.newsData.TestimonialButton}/>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        )
    }

}


export default NewsTestimonials;