import React from 'react';
import SlickSlider from 'react-slick';
import Cards from 'components/common/Cards';
import Button from 'components/common/Button';

import SliderImagePrev from '../../images/slider/slider-prev.svg'
import SliderImageNext from '../../images/slider/slider-next.svg'

class Slider extends React.Component {

    render() {

        if (!this.props.slideData) {
            return null;
        }

        function KidzNextArrow(props) {
            const {className, onClick} = props
            return (
                <div className={className} onClick={onClick}><img src={SliderImagePrev} alt="Previous Slide"/></div>
            );
        }

        function KidzPrevArrow(props) {
            const {className, onClick} = props
            return (
                <div className={className} onClick={onClick}><img src={SliderImageNext} alt="Next Slide"/></div>
            );
        }

        const settings = {
            customPaging: function(i) {
                return <a><span className="dot"></span></a>
            },
            dots: true,
            arrows: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: <KidzNextArrow />,
            prevArrow: <KidzPrevArrow />
        };

        return (
            <SlickSlider {...settings}>
                {
                    this.props.slideData.map(function (slide, i) {
                        return <div key={i}>
                            {slide.title && <h2>{slide.title}</h2>}
                            <img src={slide.imageUrl} alt={slide.imageAlt}/>
                            <div className="container">
                                <div className="row">
                                    <h3 className="strapline">{slide.strapline}</h3>
                                </div>

                                <Cards cards={slide.cardItems}/>

                                <Button button={slide.buttonData}/>

                            </div>
                        </div>

                    }, this)
                }
            </SlickSlider>
        );
    }
}

export default Slider;