import React from 'react';
import SlickSlider from 'react-slick';

class Sponsors extends React.Component {

    render() {

        if (!this.props.slideData) {
            return null;
        }

        const settings = {
            autoplay: true,
            autoplaySpeed: 3000,
            dots: false,
            arrows: false,
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: false
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }],
        };

        return (
            <SlickSlider {...settings}>
                {
                    this.props.slideData.map(function (slide, i) {
                        return <div className="sponsor-item" key={i}>
                            <img src={slide.imageUrl} alt={slide.imageAlt}/>
                        </div>

                    }, this)
                }
            </SlickSlider>
        );
    }
}

export default Sponsors;