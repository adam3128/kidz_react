import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,

} from 'react-router-dom'


import ScrollToTop from 'components/navigation/ScrollToTop';

//Route Pages
import Home   from 'components/pages/Home';
import About   from 'components/pages/About';
import NoMatch   from 'components/pages/404';

//Layout
import Header from 'components/layout/Header';
import Footer from 'components/layout/Footer';

const App = () => (
    <Router>
        <ScrollToTop>
            <div>
                <Header />
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/about" component={About}/>
                    <Route component={NoMatch}/>
                </Switch>
                <Footer />
            </div>
        </ScrollToTop>
    </Router>
);

export default App