import React from 'react';
import FooterLinks from 'components/navigation/FooterLinks'
import FooterLanguage from 'components/navigation/FooterLanguage'

const Footer = () => (
    <div>
        <FooterLinks />
        <FooterLanguage />
    </div>
);

export default Footer;