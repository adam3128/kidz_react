import React from 'react';
import TopNavigation from 'components/navigation/TopNavigation';
import MainNavigation from 'components/navigation/MainNavigation';

const Header = () => (
    <div>
        <TopNavigation />
        <MainNavigation />
    </div>
);

export default Header;