import React from 'react';
import {
    Link
} from 'react-router-dom'

class FooterLanguage extends React.Component {

    render() {

        return (

            <div className="footer-language">
                <div className="container">
                    <div className="row">
                        <div className="footer-language-options">
                            <h3>Choose your language</h3>
                            <h4>This changes the language of the site.</h4>
                            <ul>
                                <li><Link to="/">Language 1</Link></li>
                                <li><Link to="/">Language 2</Link></li>
                                <li><Link to="/">Language 3</Link></li>
                                <li><Link to="/">Language 4</Link></li>
                                <li><Link to="/">Language 5</Link></li>
                                <li><Link to="/">Language 6</Link></li>
                                <li><Link to="/">Language 7</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <hr/>

                <div className="container">
                    <div className="row">
                        <div className="footer-copyright">
                            Copyright 2017 HelpKidzLearn. All rights reserved.
                        </div>
                    </div>
                </div>

            </div>

        )
    }
}

export default FooterLanguage;