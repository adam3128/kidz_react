import React from 'react';
import FooterImage from '../../images/footer/footer-kidz-logo.svg'
import {
    Link
} from 'react-router-dom'

class FooterLinks extends React.Component {

    render() {

        return (

            <div className="footer-links">

                <div className="container">
                    <div className="row">
                        <div className="footer-column">
                            <img src={FooterImage} alt="Help Kidz Logo"/>
                        </div>
                        <div className="footer-column">
                            <h3>HKL Services</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                                <li><Link to="/about">Link 2</Link></li>
                                <li><Link to="/about">Link 3</Link></li>
                                <li><Link to="/about">Link 4</Link></li>
                                <li><Link to="/about">Link 5</Link></li>
                            </ul>
                        </div>
                        <div className="footer-column">
                            <h3>About Us</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                                <li><Link to="/about">Link 2</Link></li>
                                <li><Link to="/about">Link 3</Link></li>
                            </ul>
                        </div>
                        <div className="footer-column">
                            <h3>HelpKidzLearn</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                                <li><Link to="/about">Link 2</Link></li>
                                <li><Link to="/about">Link 3</Link></li>
                                <li><Link to="/about">Link 4</Link></li>
                                <li><Link to="/about">Link 5</Link></li>
                            </ul>
                        </div>
                        <div className="footer-column">
                            <h3>Community</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                                <li><Link to="/about">Link 2</Link></li>
                                <li><Link to="/about">Link 3</Link></li>
                                <li><Link to="/about">Link 4</Link></li>
                                <li><Link to="/about">Link 5</Link></li>
                                <li><Link to="/about">Link 6</Link></li>
                                <li><Link to="/about">Link 7</Link></li>
                            </ul>
                        </div>
                        <div className="footer-column">
                            <h3>Drop Us a Line</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                            </ul>
                            <h3>Or Email</h3>
                            <ul>
                                <li><Link to="/about">Link 1</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default FooterLinks;