import React from 'react';
import Logo from '../../images/common/helpkidzlearn-logo.svg';
import {
    Link
} from 'react-router-dom'

import BasketIcon from '../../images/common/basket-icon.svg'


class MainNavigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {toggleMobile: false};
    }

    render() {

        const toggleMobileClass = this.state.toggleMobile ? 'main-navigation-show-mobile' : 'main-navigation';

        return (
            <div className="main-logo-background">
                <div className="container">
                    <div className="row">
                        <div className="main-logo">
                            <Link to="/">
                                <img src={Logo} className="App-logo" alt="logo"/>
                            </Link>
                        </div>

                        <div className={toggleMobileClass}>

                            <li className="mobile-toggle hidden-lg-up">
                                <a href="javascript:void(0);" onClick={this.ToggleMobile.bind(this)}>Menu &#9776;</a>
                            </li>

                            <ul>
                                <li><Link to="/about">Online Services</Link></li>
                                <li><Link to="/about">Shop</Link></li>
                                <li><Link to="/about">Apps</Link></li>
                                <li><Link to="/about">News <span className="new-post">1</span></Link></li>
                                <li><Link to="/about"><img className="basket-icon" src={BasketIcon} alt="Basket Icon"/>Basket <span className="basket-items">1 x Item</span></Link></li>
                                <li className="hidden-lg-up"><Link to="/about">Login</Link></li>
                                <li className="hidden-lg-up"><Link to="/about">Sign up</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    ToggleMobile() {
        this.setState({
            toggleMobile: !this.state.toggleMobile,
        });
    }

}

export default MainNavigation;