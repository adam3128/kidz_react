import React from 'react';
import {
    Link
} from 'react-router-dom'


const TopNavigation = () => (
    <div className="top-navigation-background">
        <div className="container">
            <div className="row">
                <div className="top-navigation">
                    <ul>
                        <li><Link to="/">Login</Link></li>
                        <li><Link to="/about">Sign up</Link></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
);

export default TopNavigation;