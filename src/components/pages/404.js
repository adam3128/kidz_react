import React from 'react';

class NoMatch extends React.Component {

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="page-404">
                        Page not found - 404. Sorry please try another.
                    </div>
                </div>
            </div>
        );
    }
}


export default NoMatch;