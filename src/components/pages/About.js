import React from 'react';
import Slider from 'components/common/Slider';
import Sponsors from 'components/common/Sponsors';

import SlideImage from '../../images/slider/slider-games.jpg'
import SlideImage2 from '../../images/slider/slider-maker.jpg'

import CardImage from '../../images/cards/cards-1.svg'
import CardImage2 from '../../images/cards/cards-2.svg'
import CardImage3 from '../../images/cards/cards-3.svg'
import CardImage4 from '../../images/cards/cards-4.svg'

import SponsorImage1 from '../../images/sponsors/sponsor-1.svg'
import SponsorImage2 from '../../images/sponsors/sponsor-2.svg'
import SponsorImage3 from '../../images/sponsors/sponsor-3.svg'
import SponsorImage4 from '../../images/sponsors/sponsor-4.svg'
import SponsorImage5 from '../../images/sponsors/sponsor-5.svg'

class DefaultPage extends React.Component {

    render() {

        const homeSlideData = [{
            title: "About Page!",
            imageUrl: SlideImage2,
            imageAlt: "Games & Activities Information",
            strapline: "Fun, accessible games & activities for young children and those with learning difficulties to play online!",
            cardItems: [{
                title: "Universal Learning",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Accessible & Fun",
                imageUrl: CardImage2,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Readymade Learning",
                imageUrl: CardImage3,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Classroom Education",
                imageUrl: CardImage4,
                imageAlt: "Classroom Education",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }],
            buttonData: {
                introduction: "Join over 66,000 teachers and therapists using HelpKidzLearn",
                title: "Order Now",
                link: "/product",
            },
        }, {
            imageUrl: SlideImage,
            imageAlt: "Games & Activities Information",
            strapline: "Fun, accessible games & activities for young children and those with learning difficulties to play online!",
            cardItems: [{
                title: "Universal Learning",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Classroom Education",
                imageUrl: CardImage,
                imageAlt: "Classroom Education",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }],
            buttonData: {
                introduction: "Join over 66,000 teachers and therapists using HelpKidzLearn",
                title: "Order Now",
                link: "/product",
            },
        }];

        const sponsorSlideData = [{
            imageUrl: SponsorImage1,
            imageAlt: "Games & Activities Information",
        }, {
            imageUrl: SponsorImage2,
            imageAlt: "Games & Activities Information",
        }, {
            imageUrl: SponsorImage3,
            imageAlt: "Games & Activities Information",
        }, {
            imageUrl: SponsorImage4,
            imageAlt: "Games & Activities Information",
        }, {
            imageUrl: SponsorImage5,
            imageAlt: "Games & Activities Information",
        }, {
            imageUrl: SponsorImage3,
            imageAlt: "Games & Activities Information",
        }];


        return (
            <div className="main-page-content">

                <Slider slideData={homeSlideData}/>

                <div className="sponsor-slider container">
                    <div className="row">
                        <Sponsors slideData={sponsorSlideData}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default DefaultPage;