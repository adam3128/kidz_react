//React
import React from 'react';

//Components
import Slider from 'components/common/Slider';
import Sponsors from 'components/common/Sponsors';
import NewsTestimonials from 'components/common/NewsTestimonials';

//Images
import SlideImage from '../../images/slider/slider-games.jpg'
import SlideImage2 from '../../images/slider/slider-maker.jpg'

import CardImage from '../../images/cards/cards-1.svg'
import CardImage2 from '../../images/cards/cards-2.svg'
import CardImage3 from '../../images/cards/cards-3.svg'
import CardImage4 from '../../images/cards/cards-4.svg'

import SponsorImage1 from '../../images/sponsors/sponsor-1.svg'
import SponsorImage2 from '../../images/sponsors/sponsor-2.svg'
import SponsorImage3 from '../../images/sponsors/sponsor-3.svg'
import SponsorImage4 from '../../images/sponsors/sponsor-4.svg'
import SponsorImage5 from '../../images/sponsors/sponsor-5.svg'

class Home extends React.Component {

    render() {

        //Data that would typically be build in from an API in the component lifecycle
        const homeSlideData = [{
            title: "Games & Activities",
            imageUrl: SlideImage,
            imageAlt: "Games & Activities Information",
            strapline: "Fun, accessible games & activities for young children and those with learning difficulties to play online!",
            cardItems: [{
                title: "Universal Learning",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Accessible & Fun",
                imageUrl: CardImage2,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Readymade Learning",
                imageUrl: CardImage3,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Classroom Education",
                imageUrl: CardImage4,
                imageAlt: "Classroom Education",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }],
            buttonData: {
                introduction: "Join over 66,000 teachers and therapists using HelpKidzLearn",
                title: "Order Now",
                link: "/about",
            },
        }, {
            imageUrl: SlideImage2,
            imageAlt: "Games & Activities Information",
            strapline: "Fun, accessible games & activities for young children and those with learning difficulties to play online!",
            cardItems: [{
                title: "Universal Learning",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }, {
                title: "Classroom Education",
                imageUrl: CardImage2,
                imageAlt: "Classroom Education",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
            }],
            buttonData: {
                introduction: "Join over 66,000 teachers and therapists using HelpKidzLearn",
                title: "Order Now",
                link: "/product",
            },
        }];

        const sponsorSlideData = [{
            imageUrl: SponsorImage1,
            imageAlt: "Sponsor 1",
        }, {
            imageUrl: SponsorImage2,
            imageAlt: "Sponsor 2",
        }, {
            imageUrl: SponsorImage3,
            imageAlt: "Sponsor 3",
        }, {
            imageUrl: SponsorImage4,
            imageAlt: "Sponsor 4",
        }, {
            imageUrl: SponsorImage5,
            imageAlt: "Sponsor 5",
        }, {
            imageUrl: SponsorImage3,
            imageAlt: "Sponsor 3",
        }];

        const NewsTestimonialData = {
            NewsItems: [{
                title: "HelpKidzLearn in eight Languages",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam et laoreet enim mi vel blandit.",
                strapline: "27th July 2017",
            }, {
                title: "Inclusive EyeGaze Foundations",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam et laoreet enim mi vel blandit.",
                strapline: "27th July 2017",
            }],
            NewsButton: {
                colour: "orange",
                title: "Read More",
                link: "/about",
            },
            TestimonialItems: [{
                title: "Plain fun for everyone!",
                imageUrl: CardImage,
                imageAlt: "Games & Activities Information",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
                strapline: "Barbara, Spotsylvania County Schools",
            }, {
                title: "Accessible & Fun",
                imageUrl: CardImage,
                imageAlt: "“HKL offers such a variety of activities!”",
                content: "Lorem ipsum dolor sit amet elit. Uimperdiet vulputate sem, id pellentesque felis cursus euedturpis.",
                strapline: "Jennifer Schkade, Klein Independent School",
            }],
            TestimonialButton: {
                colour: "orange",
                title: "Read More",
                link: "/about",
            }
        };


        return (
            <div className="main-page-content">

                <Slider slideData={homeSlideData}/>

                <NewsTestimonials newsData={NewsTestimonialData}/>

                <div className="sponsor-slider container">
                    <div className="row">
                        <Sponsors slideData={sponsorSlideData}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default Home;